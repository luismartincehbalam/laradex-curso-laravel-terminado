@extends('layouts.app')

@section('title', 'Trainers')

@section('content')

@include('common.success')
<center>
    <h1 style="margin: top ">Esta es la lista de los entrenadores pokemones </h1>
</center>
<body style="background-color: aquamarine ">
<div class="container" >
<div class="row" >
@foreach ($trainers as $trainer)

    <div class="col-sm">
<div class="card text-center" style="width: 18rem; margin-top:70px;">
    <img style="width:100px; height:100px; background:#efefef; margin:20px;" class="card-img-top rounded-circle mx-auto d-block" src="images/{{$trainer->avatar}}" >
    <div class="card-body">
      <h5 class="card-title">{{$trainer->name}}</h5>
      <p class="card-text">{{$trainer->description}}</p>
      <a href="/trainers/{{$trainer->slug}}" class="btn btn-primary">ver entrenador</a>
        </div>
    </div>

</div>
</body>

    @endforeach
@endsection