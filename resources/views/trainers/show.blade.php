@extends('layouts.app')

@section('title', 'Trainer')

@section('content')

@include('common.success')
<body style="background-color: aquamarine ">
    <center>
    <h1 style="margin: top ">Este es tu informacion de la pagina pokemon </h1>
</center>
    <div class="text-center">
<img style="width:200px; height:200px; background:#efefef; margin:20px;" class="card-img-top rounded-circle mx-auto d-block" src="../images/{{$trainer->avatar}}" >
<h5>{{$trainer->name}}</h5>

<p>{{$trainer->description}}</p>

<a href="/trainers/{{$trainer->slug}}/edit" class="btn btn-primary">Editar</a>

<form class="form-group" method="POST" action="/trainers/{{$trainer->slug}}/" enctype="multipart/form-data" style="margin: 10px">   
    @method('DELETE')
    @csrf <!--Funcion de seguridad-->
<button type="submit" class="btn btn-danger" style="margin: 10px">Eliminar</button>
</form>

</div>
</body>
@endsection