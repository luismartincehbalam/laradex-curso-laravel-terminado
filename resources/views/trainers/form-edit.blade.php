<body style="background-color: aquamarine ">
<div class="form-group" >
    <form class="form-group" method="POST" action="/trainers/{{$trainer->slug}}" enctype="multipart/form-data" style="margin: 10px">        
        @method('PUT')
        @csrf <!--Funcion de seguridad-->
        <label for="">Nombre</label>
        <input type="text"  name="name" style="margin: 10px" class="form-control" value="{{$trainer->name}}">
        <label for="">Slug</label>
        <input type="text" name="slug" style="margin: 10px"  class="form-control" value="{{$trainer->slug}}" >
        <label for="">Avatar</label> 
        <input type="text" name="avatar" style="margin: 10px" value="{{$trainer->avatar}}" disabled>
        <input type="file" name="avatar" >
        
        <br>
        <label for="">Descripcion</label>
        <input type="text" name="description" style="margin: 10px" value="{{$trainer->description}}" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary" style="margin: 10px">Actualizar</button>
    </form>
</body>